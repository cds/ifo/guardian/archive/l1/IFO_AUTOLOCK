from guardian import GuardState
import time
import cdsutils

nominal = 'AUTOLOCK_ON'


class INIT(GuardState):
    index = 0
    request = True

    def main(self):
        return True

    def run(self):
        return True


class AUTOLOCK_OFF(GuardState):
    index = 10
    request = True

    def main(self):
        return True

    def run(self):
        notify('ISC Guardian will not try to lock IFO')
	return True


class AUTOLOCK_ON(GuardState):
    index = 20
    request = True

    def main(self):
        return True

    def run(self):
        return True

##################################################################

edges = [
    ('INIT','AUTOLOCK_OFF'),
    ('AUTOLOCK_OFF', 'AUTOLOCK_ON'),
    ('AUTOLOCK_ON', 'AUTOLOCK_OFF')
]
